class Hero {
  constructor({ name, power, identification }) {
    this.name = name
    this.power = power
    this.id = identification
  }
}

module.exports = Hero