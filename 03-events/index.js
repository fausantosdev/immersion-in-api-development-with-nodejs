const EventEmitter = require('events')

class MeuEmissor extends EventEmitter {

}

const meuEmissor = new MeuEmissor()

const nomeEvento = 'usuario:click'

// Obiservador
meuEmissor.on(nomeEvento, function(click) {
    console.log('um usuário clicou: ', click)
})

const stdin = process.openStdin()

stdin.addListener('data', function (value) {
    console.log(`você digitou: ${value.toString().trim()}`)
})

// Emissor(emite o evento)
//meuEmissor.emit(nomeEvento, 'na barra de rolagem')
//meuEmissor.emit(nomeEvento, 'no ok')
//meuEmissor.emit(nomeEvento, 'no rabão da gótica gostosa')

//let count = 0

//setInterval(function() {
//    meuEmissor.emit(nomeEvento, `comeu o cú da gótica rabuda ${count++} vezes`)
//}, 1000)