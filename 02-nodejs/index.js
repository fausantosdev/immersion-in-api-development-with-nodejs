/**
 * 0 Obter um usuário
 * 1 Obter o número de telefone de um usuário a partir de seu Id
 * 2 Obter o endereço do usuário pelo Id
 */

// Importamos um módulo internodo NodeJS
const util = require('util')

function getUser () {
    // Se sucesso -> resolve
    // Se erro    -> reject 
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            //return reject(new Error('Deu xabu man...'))

            return resolve({
                id: 1,
                name: 'João',
                dateOfBirth: new Date()
            })
        }, 1000)
    })
}

// Por padrão, o callback é sempre o último parâmetro
function getPhone (userId) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            return resolve({
                phone: '888442211',
                ddd: '22'
            })
        }, 2000)
    })
}

const getAddressAsync = util.promisify(getAddress)

function getAddress (userId, callback) {
    setTimeout(function() {
        return callback(null, {
            street: 'Rua dos Bobos',
            number: '0'
        })
    }, 2000)
}

// 1° - Adicionar o async na função -> automaticamente ela retornará uma promisse
async function main () {
    try {
        console.time('medida-promisse')
        const user = await getUser()
        //const phone = await getPhone(user.id)
        //const address = await getAddressAsync(user.id)
        
        const result = await Promise.all([
            getPhone(user.id),
            getAddressAsync(user.id)
        ]) 

        const phone = result[0]
        const address = result[1]

        console.log(user)
        console.log(phone)
        console.log(address)
        console.timeEnd('medida-promisse')
    } catch (error) {
        console.error('Deu ruim: ', error)
    }
}

main()

//const userPromisse = getUser()
// Para manipular o sucesso usamos .then
// Para manipular o error usamos .catch
// user -> phone -> (phone)
//userPromisse
    //.then(function resolveUser(resultUser) {
    //    console.log('- step 1')

    //    return getPhone(resultUser.id)
    //        .then(function resolvePhone(resultPhone) {    
    //            return {
    //                user: {
    //                    id: resultUser.id,
    //                    name: resultUser.name,
    //                },
    //                phone: resultPhone
    //            }
    //        })
    //})
    //.then(function resolveUser(resultUser_2) {
    //    console.log('- step 2')

    //    const address = getAddressAsync(resultUser_2.user.id)
        
    //    return address
    //        .then(function resolveAdress(resultAddress) {
    //            return {
    //                user: resultUser_2.user,
    //                phone: resultUser_2.phone,
    //                address: resultAddress
    //            }
    //        })
    //})
    // OBS: o próximo then terá o resultado do último que foi manipulado
    //.then(function resolveUser(resultUser_3) {
    //    console.log('- step 3')
    //    console.log(resultUser_3)
        //console.log('Deu bom: ', result)
    //})
    //.catch(function(error) {
    //    console.log('- fail :(')
    //    console.error('Deu ruim: ', error)
    //})

/*
getUser(function resolveUser(erro, user) {
     null || '' || 0 === false
    if (erro) {
        console.error('Erro in user: ', erro)
        return
    }

    getPhone(user.id, function resolvePhone(erro1, phone) {
        if (erro1) {
            console.error('Erro in phone: ', erro)
            return
        }

        getAddress(user.id, function resolveAddress(erro2, address) {
            if (erro2) {
                console.error('Erro in address: ', erro)
                return
            }

            console.log(`
                Nome: ${user.name}
                Data de Nascimento: ${user.dateOfBirth}
                Telefone: (${phone.ddd}) ${phone.phone}
                Endereço: ${address.street} - ${address.number}
            `)
        })
    })
})
*/
//const phone = getPhone(user.id)

//console.log('Telefone: ', phone)