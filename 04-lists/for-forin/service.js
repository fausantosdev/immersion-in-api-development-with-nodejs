const axios = require('axios')

const URL = `https://swapi.dev/api/people`

async function getPeoples () {
    const url = URL
    const response = await axios.get(url)

    return response.data
}

module.exports = {
    getPeoples
}