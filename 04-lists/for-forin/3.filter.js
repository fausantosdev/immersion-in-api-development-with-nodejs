const { getPeoples } = require('./service')

/*
    // Destructuring 
    const item = {
        name: 'Fau',
        age: 33
    }

    const { name } = item

    console.log(item)
 */

Array.prototype.myFilter = function(callback) {
    const lista = []

    for(index in this) {
        const item = this[index]
        const result = callback(item, index, this)

        if(!result) continue

        lista.push(item)
    }

    return lista
}

async function main() {
    try {
        const { results } = await getPeoples()

        //const familyLars = results.filter(function(item) {
            /*
                Por padrão precisa retornar um booleano
                para informar se deve manter ou remover da lista

                false remove, true mantém
            */

           // Não encontrou: -1, encontrou: posição no array
        //   const result = item.name.toLowerCase().indexOf('lars') !== -1
           
        //    return result 
        //})

        const familyLars = results.myFilter((item, index, list) => {
            console.log(`Index: ${index} - ${list.length}`)
            return item.name.toLowerCase().indexOf('lars') !== -1
        })

        const names = familyLars.map(people => people.name)

        console.log(names)

    } catch (error) {
        console.error('Deu ruim: ', error)
    }
}

main()