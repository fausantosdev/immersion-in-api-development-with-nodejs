const { getPeoples } = require('./service')

Array.prototype.myReduce = function(callback, initial_valuel) {
    let end_value = typeof initial_valuel !== undefined ? initial_valuel : this[0]

    for(let index = 0; index <= this.length; index++){
        end_value = callback(end_value, this[index], this)
    }

    return end_value
}

async function main() {
    try {
        const { results } = await getPeoples()

        const weights = results.map(item => parseInt(item.mass))
        // [20, 5, 7, 70, 78] = 0
        
        //console.log(weights)

        //const total = [].reduce((prev, next) => {
        //    return prev + next
        //}, 0)
        // OBS: array.reduce(callback, initial_value)
        const myList = [
            ['Fulado', 'Felozo'],
            ['FilantroPR', 'AláEle']
        ]

        const total = myList.myReduce((prev, next) => {
            return prev.concat(next)
        }, []).join(', ')

        console.log(total)

    } catch (error) {
        console.error('Deu ruim: ', error)
    }
}

main()