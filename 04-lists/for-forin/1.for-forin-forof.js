const { getPeoples } = require('./service')

async function main() {
    try {
        const { count, next, results } = await getPeoples('')

        const names = []

        console.time('for')
        for(let i=0; i < results.length; i++) {
            const people = results[i]
            names.push(people.name)
        }
        console.timeEnd('for')

        console.time('forin')
        for(let i in results){
            const people = results[i]
            names.push(people.name)
        }
        console.timeEnd('forin')

        console.time('forof')
        for(people of results){
            names.push(people.name)
        }
        console.timeEnd('forof')

        console.log(names)

    } catch (error) {
        console.error('Erro interno: ', error)    
    }
}

main()