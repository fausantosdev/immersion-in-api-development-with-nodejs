const { getPeoples } = require('./service')

Array.prototype.myMap = function(callback) {
    const newMappedArray = []

    for(let index = 0; index <= this.length -1; index++){
        const result = callback(this[index], index)    
        newMappedArray.push(result)
    }

    return newMappedArray
}

async function main() {
    try {
        const { results } = await getPeoples()

        //const names = []
        
        /*results.forEach(people => {
            names.push(people.name)
        })*/
        /*const names = results.map(function (people) {
            return people.name
        })*/
        //const names = results.map(people => people.name)

        const names = results.myMap(function(people, index){
            return `${index}-${people.name}`
        })

        console.log('Names: ', names)

    } catch (error) {
        console.error('Deu ruim: ', error)
    }
}

main()