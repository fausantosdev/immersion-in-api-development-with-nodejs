const { Command } = require('commander')

const database = require('./database')

const Hero = require('./hero')

async function main() {
  const program = new Command()

  program
    .version('v1')
    .option('-n, --name [value]', 'Hero name')
    .option('-p, --power [value]', 'Hero power')
    .option('-id, --identification [value]', 'Hero ID')

    .option('-r, --register', 'Register hero')
    .option('-l, --list', 'List the heroes')
    .option('-rm, --remove', 'Remove a hero by id')
    .option('-ed, --edit [value]', 'Edit a hero by id')

    .parse(process.argv)

  try {
    const options = program.opts()
 
    const hero = new Hero(options)

    if(options.register) {
      delete hero.id

      const result = await database.create(hero)
      
      if(!result) {
        console.error('Unregistered hero')
        return
      }

      console.log('Hero successfully registered')
    }  

    if(options.list) {
      const result = await database.list()

      console.log(result)
      return
    }

    if(options.remove) {
      const result = await database.delete(hero.id)

      if(!result) {
        console.error('Unable to remove hero')
        return
      }

      console.log('Hero successfully removed')
    }

    if(options.edit) {
      const idToUpdate = parseInt(options.edit)

      // Remover todas as chaves undefined | null
      const data = JSON.stringify(hero)

      const updateHero = JSON.parse(data)

      const result = await database.edit(idToUpdate, updateHero)

      if(!result) {
        console.error('Unable to update hero')
        return
      }

      console.log('Hero successfully updated')
    }

  } catch (error) {
    console.error('Deu ruim: ', error)
  }
}

main()