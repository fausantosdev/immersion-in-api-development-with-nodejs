const { deepEqual, ok } = require('assert')

const database = require('./database')

const DEFAULT_REGISTER_ITEM = {
  id: 1,
  name: 'Flash',
  power: 'Speed'
}

const DEFAULT_UPDATE_ITEM = {
  id: 2,
  name: 'Lanterna Verde',
  power: 'Constructors'
}

describe('Hero Manipulation Suite', () => {
  before(async () => {
    await database.create(DEFAULT_REGISTER_ITEM)
    await database.create(DEFAULT_UPDATE_ITEM)
  })

  after(async () => {
    await database.delete()
  })

  it('Should search for a hero in the archives', async () => {
    const expected = DEFAULT_REGISTER_ITEM

    const [ result ] = await database.list(expected.id)
    
    deepEqual(result, expected)
  })
  
  it('Should register a hero using files', async () => {
    const expected = DEFAULT_REGISTER_ITEM

    const result = await database.create(DEFAULT_REGISTER_ITEM)

    const [ current ] = await database.list(DEFAULT_REGISTER_ITEM.id)

    deepEqual(current, expected)
  })

  it('Should remove a hero by id', async () => {
    const expected = true

    const result = await database.delete(DEFAULT_REGISTER_ITEM.id)

    deepEqual(result, expected)
  })

  it('Should update a hero by id', async () => {
    const expected = {
      ...DEFAULT_UPDATE_ITEM,
      name: 'Batman',
      power: 'Dinheiro'
    }

    const newData = {
      name: 'Batman',
      power: 'Dinheiro'
    }

    await database.edit(DEFAULT_UPDATE_ITEM.id, newData)

    const [ result ] = await database.list(DEFAULT_UPDATE_ITEM.id)

    deepEqual(result, expected)
  })
})