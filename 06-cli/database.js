const { readFile, writeFile } = require('fs')
const { promisify } = require('util')

const writeFileAsync = promisify(writeFile)
const readFileAsync = promisify(readFile)

/**
 * OBS: o arquivo json poderia ser importado diretamente. 
 */

class Database {
  constructor() {
    this.FILE_NAME = 'heroes.json'
  }
  
  async create(hero) {
    const data = await this.#getDataFile()

    const id = hero.id <= 2 ? hero.id : Date.now()

    const heroWithId = {
      id,
      ...hero
    }

    const newData = [
      ...data, 
      heroWithId
    ]

    const result = await this.#writeToFile(newData)

    return result
  }

  async list(id) {
    const data = await this.#getDataFile()
    const filteredData = data.filter(item => id ? item.id === id : true)
    return filteredData 
  }

  async edit(id, modifications) {
    const data = await this.#getDataFile()

    const index = data.findIndex(item => item.id === parseInt(id))

    if(index === -1) {
      throw Error('O herói informado não existe')
    }

    const current = data[index]

    const updateObject = {
      ...current,
      ...modifications
    }

    data.splice(index, 1)

    return await this.#writeToFile([
      ...data,
      updateObject
    ])
  }

  async delete(id) {
    if(!id) {
      return this.#writeToFile([])
    }

    const data = await this.#getDataFile()

    const index = data.findIndex(item => item.id === parseInt(id))

    if(index === -1) {
      throw new Error('O usuário informado não existe')
    }

    data.splice(index, 1)

    return await this.#writeToFile(data)
  }

  async #writeToFile(data) {
    await writeFileAsync(this.FILE_NAME, JSON.stringify(data))
    return true
  }

  async #getDataFile() {
    const file = await readFileAsync(this.FILE_NAME, 'utf8')
    return JSON.parse(file.toString())
  }
}

module.exports = new Database()