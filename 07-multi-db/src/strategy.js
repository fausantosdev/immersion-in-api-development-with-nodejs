class NotImplementedException extends Error {
    constructor() {
        super('Not Implemented Exception')
    }
}

// Simula a interface
// Quando extendida, caso a classe filha não tenha seus métodos implementados, usará os dela, o que irá estourar a excessão
class ICrud {
    create(item) {
        throw new NotImplementedException()
    }

    read(query) {
        throw new NotImplementedException()
    }

    update(id, item) {
        throw new NotImplementedException()
    }

    delete(id) {
        throw new NotImplementedException()
    }
}

// Caso ele não tenha o método implementado, ele usará o da classe mãe, e vai estourar o NotImplementedException
class MongoDB extends ICrud {
    // Por padrão no javascript, quando se extende uma classe, é necessário chamar 
    constructor() {
        super()// Invocar o construtor da classe extendida
    }
    /**
     * OBS: 
     * Não é obrigatório chamar o super() no construtor de uma classe filha. 
     * No entanto, é uma boa prática chamar o super() para inicializar os atributos
     * e métodos da classe pai.
     */

    create(item) {
        console.log('O item foi salvo em MongoDB')
    }
}

class Postgres extends ICrud {
    constructor() {
        super()
    }

    create(item) {
        console.log('O item foi salvo em Postgres')
    }
}

class ContextStrategy {
    #_database

    constructor(strategy) {
        this.#_database = strategy
    }

    create(item) {
        return this.#_database.create(item)
    }

    read(query) {
        return this.#_database.read(query)
    }

    update(id, item) {
        return this.#_database.update(id, item)
    }

    delete(id) {
        return this.#_database.delete(id)
    }
}

const contextMongo = new ContextStrategy(new MongoDB())

contextMongo.create()
// contextMongo.read()

const contextPostgres = new ContextStrategy(new Postgres())

contextPostgres.create()