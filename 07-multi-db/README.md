## ---- Postgres

docker run \
    --name postgres \
    -e POSTGRES_USER=fausantosdev \
    -e POSTGRES_PASSWORD=postgres \
    -e POSTGRES_DB=heroes \
    -p 5432:5432 \
    -d \
    postgres

docker ps

docker exec -it postgres /bin/bash

docker run \
    --name adminer \
    -p 8080:8080 \
    --link postgres:postgres \
    -d \
    adminer

## ---- MingoDB

docker run \
    --name mongodb \
    -p 27017:27017 \
    -e MONGO_INITDB_ROOT_USERNAME=admin \
    -e MONGO_INITDB_ROOT_PASSWORD=admin \
    -d \
    mongo:6

docker ps

docker run \
    --name mongoclient \
    -p 3000:3000 \
    --link mongodb:mongodb \
    -d \
    mongoclient/mongoclient

docker exec -it mongodb \
    mongo --host localhost -u admin -s admin --authenticationDatabase \
    --eval "db.getSiblingDB('heroes').createUser({user:'fausantosdev',pwd:'admin',roles:[{role:'readWrite',db:'heroes'}]})"

## ---- OBS:


O comando docker exec -it mongodb é usado para executar um comando dentro de um container Docker. No caso específico, o comando mongodb é usado para iniciar a shell do MongoDB dentro do container com o nome mongodb.

O comando docker exec é composto por três partes:

docker: é o nome do comando Docker.

exec: é a palavra-chave que indica que o comando que será executado será dentro de um container Docker.

-it: são as opções do comando exec. A opção -i indica que o terminal será conectado ao container, e a opção -t indica que um pseudo-terminal será criado para o container.

O comando mongodb é o nome do comando que será executado dentro do container. No caso específico, esse comando é usado para iniciar a shell do MongoDB.

Portanto, o comando docker exec -it mongodb pode ser interpretado como:

Execute o comando mongodb dentro do container com o nome mongodb, conectando o terminal ao container e criando um pseudo-terminal para o container.

Após executar esse comando, você será conectado à shell do MongoDB dentro do container. Você poderá então usar a shell para interagir com o banco de dados MongoDB.

Por exemplo, você pode usar a shell para criar uma nova coleção, inserir dados em uma coleção ou consultar dados de uma coleção.

Aqui está um exemplo de como usar o comando docker exec -it mongodb:

docker exec -it mongodb

Isso iniciará a shell do MongoDB dentro do container com o nome mongodb. Você poderá então usar a shell para interagir com o banco de dados MongoDB.

Aqui está outro exemplo de como usar o comando docker exec -it mongodb:

docker exec -it mongodb mongo

Isso também iniciará a shell do MongoDB dentro do container com o nome mongodb. No entanto, o comando mongo é usado para iniciar a shell do MongoDB. O comando mongo é equivalente ao comando docker exec -it mongodb.

É importante notar que o comando docker exec -it mongodb só pode ser usado se o container mongodb estiver em execução. Se o container não estiver em execução, o comando falhará.
